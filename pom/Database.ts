import sql from 'mssql';

export class Database{
    readonly dbConfins = {
        user: 'username',
        password: 'userpassword',
        database: 'database_name',
        server: 'ip_server',
        options: {
          encrypt: false,
          trustServerCertificate: false
        }
    }

    readonly dbStaging = {
        user: 'username',
        password: 'userpassword',
        database: 'database_name',
        server: 'ip_server',
        options: {
          encrypt: false,
          trustServerCertificate: false
        }
    }

    async connectToDatabase(sqlConfig: any){
        await sql.connect(sqlConfig);
    }

    async getData(){
        await this.connectToDatabase(this.dbStaging);
        const data = await sql.query(`Select top 10 * From STG_MAIN`);

        return data;
    }
}