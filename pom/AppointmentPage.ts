import { type Locator, type Page } from "@playwright/test";

export class AppointmentPage {
    readonly page: Page;
    readonly slc_facility: Locator;
    readonly chk_hospital_readmission: Locator;
    readonly chk_healthcare_program_none: Locator;
    readonly chk_healthcare_program_medicaid: Locator;
    readonly chk_healthcare_program_medicare: Locator;
    readonly txt_visit_date: Locator;
    readonly txt_comment: Locator;
    readonly btn_submit_appointment: Locator;

    constructor(page: Page){
        this.page = page;
        this.slc_facility = page.getByLabel('Facility');
        this.chk_hospital_readmission = page.getByLabel('Apply for hospital readmission');
        this.chk_healthcare_program_none = page.getByLabel('None');
        this.chk_healthcare_program_medicaid = page.getByLabel('Medicaid');
        this.chk_healthcare_program_medicare = page.getByLabel('Medicare');
        this.txt_visit_date = page.locator("xpath=//input[@id='txt_visit_date']");
        this.txt_comment = page.getByPlaceholder('Comment');
        this.btn_submit_appointment = page.getByRole('button', { name: 'Book Appointment' });
    }

    async selectHospitalFacility(v_facility: string){
        await this.slc_facility.selectOption(v_facility);
    }

    async agreeHospitalReadmission(){
        await this.chk_hospital_readmission.check();
    }

    async selectHealthcareProgram(v_healthcare_program: string) {
        if(v_healthcare_program == 'Medicare'){
            await this.chk_healthcare_program_medicare.check();
        } else if(v_healthcare_program == 'Medicaid') {
            await this.chk_healthcare_program_medicaid.check();
        } else {
            await this.chk_healthcare_program_none.check();
        }
    }

    async setVisitDate(v_visit_date: string){
        await this.txt_visit_date.pressSequentially(v_visit_date);
    }

    async setComment(v_comment: string){
        await this.txt_comment.fill(v_comment);
    }

    async clickButtonSubmit(){
        await this.btn_submit_appointment.click();
    }

    async submitAppointment(facility: string, heathcare_program: string, visit_date: string, comment: string){
        await this.selectHospitalFacility(facility);
        await this.agreeHospitalReadmission();
        await this.selectHealthcareProgram(heathcare_program);
        await this.setVisitDate(visit_date);
        await this.setComment(comment);
        await this.clickButtonSubmit();
    }
}
